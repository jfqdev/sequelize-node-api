//----->>> Libraries & Tools <<<------
const express = require('express');
const app = express();

//--------->>> Modules <<<------------
// Getting routes manager.
const index = require('./routes/index');
//Sequelize object
const sequelize = require('./db/db');
//Model associations
require('./db/associations');

//Cors Config
//const cors = require('cors');
//app.use(cors());

//JSON Parser.
app.use(express.json())

// Mounting our routes manager.
app.use(index);


//Express Server
app.listen(3001, () => {
    console.log('Listening to port 3001');

    //Connect to dB
    sequelize.sync({ force: false }).then(() => {
        console.log('Connected to Mysql db');
    }).catch(error => {
        console.log('An error has occur', error);
    })

});