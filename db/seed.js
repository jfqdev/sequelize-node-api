const sequelize = require('./db');
const Post = require('./models/Post');
const Category = require('./models/Category');
require('./associations');


// Categories
const categories = [
    { descripcion: "Miscelanea" },
    { descripcion: "Noticias" },
    { descripcion: "Deportes" },
    { descripcion: "Entretenimiento" },
    { descripcion: "Educacion" }
];

// Posts
const posts = [
    { titulo: "Titulo del Post", contenido: "est necessitatibus architecto usssssssst lab  orum", imagen: 'https://via.placeholder.com/600/92c952', categoryId: 3 },
    { titulo: "Titulo del Post", contenido: "mollitia soluta ut rerum eos alissssssssssssssquam", imagen: 'https://via.placeholder.com/600/771796', categoryId: 3 },
    { titulo: "Titulo del Post", contenido: "laboriosam odit nam necessitatibus essssss ssitati", imagen: 'https://via.placeholder.com/600/d32776', categoryId: 5 },
    { titulo: "Titulo del Post", contenido: "et ea illo et sit voluptas animi blanditi is porro", imagen: 'https://via.placeholder.com/600/51aa97', categoryId: 1 },
    { titulo: "Titulo del Post", contenido: "et ea illo et sit voluptas animi blanditi is porro", imagen: 'https://via.placeholder.com/600/61a65', categoryId: 1 },
    { titulo: "Titulo del Post", contenido: "laboriosssssssam odit ssnam necessssssss  itatibus", imagen: 'https://via.placeholder.com/600/fdf73e', categoryId: 2 },
    { titulo: "Titulo del Post", contenido: "mollitia soluta ut rerum tatibus assssss  ssssrch ", imagen: 'https://via.placeholder.com/600/56acb2', categoryId: 2 },
];


sequelize.sync({ force: true }).then(() => {
    // Conexión establecida
    console.log("Connection made...");
}).then(() => {
    // Insert Categories
    categories.forEach(category => Category.create(category));
}).then(() => {
    // Insert Posts
    posts.forEach(post => Post.create(post));
});