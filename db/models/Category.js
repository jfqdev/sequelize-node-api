const { DataTypes, Model } = require('sequelize');

const sequelize = require('../db');

class Category extends Model {}

Category.init({
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    descripcion: {
        type: DataTypes.STRING,
        allowNull: false
    }

}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    timestamps: false,
    modelName: 'category' // We need to choose the model name
});

module.exports = Category;