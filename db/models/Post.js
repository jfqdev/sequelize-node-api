const { DataTypes, Model } = require('sequelize');

const sequelize = require('../db');

class Post extends Model {}

Post.init({
    // Model attributes are defined here
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    titulo: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    contenido: {
        type: DataTypes.STRING,
        allowNull: false
    },
    imagen: {
        type: DataTypes.STRING,
        allowNull: false
    }


}, {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    timestamps: true,
    updatedAt: false,
    createdAt: "fecha",
    modelName: 'post' // We need to choose the model name
});

module.exports = Post;