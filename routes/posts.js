//----->>> Libraries & Tools <<<------
const express = require('express');
const router = express.Router();

//----->>> dB Model <<<------
const Post = require('../db/models/Post');
const Category = require('../db/models/Category');

//----->>> Middleware <<<------
const isValidImage = require('../middleware/isValidImage')
const checkPostInDB = require('../middleware/checkPostInDB')


//----------->> GET <<------------

//Get all posts.
router.get('/', async(req, res) => {

    try {
        const resDB = await Post.findAll({
            order: [
                ['fecha', 'DESC']
            ],
            include: {
                model: Category,
                attributes: ['id', 'descripcion']
            },
            attributes: ['id', 'titulo', 'imagen', 'fecha']
        })
        res.json(resDB);

    } catch (err) {
        res.json({ error: err })
    }

})

//Get a single post by id.
router.get('/:id', checkPostInDB, async(req, res, next) => {

    const { searchDB } = res;

    try {
        res.status(200).json(searchDB);

    } catch (err) {
        res.json({ error: err })
    }

})


//----------->> POST <<------------

//Create a new post
router.post('/', isValidImage, async(req, res) => {

    try {
        const resDB = await Post.create(
            req.body, {
                fields: ['titulo', 'contenido', 'imagen', 'categoryId']
            }
        );

        res.json(resDB);

    } catch (err) {
        res.json({ error: err })
    }

})

//----------->> PATCH <<------------

//Update a post.
router.patch('/:id', checkPostInDB, isValidImage, async(req, res) => {
    try {

        //Update record in dB
        const patchDB = await Post.update(req.body, {
                where: {
                    id: req.params.id
                },
                attributes: ['titulo', 'contenido', 'imagen', 'categoryId']
            })
            //handle object not modified.
        if (patchDB[0] === 0) {
            res.status(202).json({
                msg: `Post #${req.params.id} was found but not modified. Object must be different from the one stored in dB`
            });
            return
        }
        //Returning object after being updated.
        const resDB = await Post.findAll({
            include: {
                model: Category,
                attributes: ['id', 'descripcion']
            },
            attributes: ['id', 'titulo', 'contenido', 'imagen', 'fecha'],
            where: {
                id: req.params.id
            }
        })
        res.status(200).json(resDB);


    } catch (err) {
        res.json({ error: err })
    }

})

//----------->> DELETE <<------------

//Delete a post.
router.delete('/:id', async(req, res) => {
    try {
        const resDB = await Post.destroy({
            where: {
                id: req.params.id
            }
        });
        //Handle not found record.
        if (!resDB) {
            res.status(404).json({ error: `Post #${req.params.id} not found` });
        }
        //Handle found and deleted record
        else {
            res.json({ msg: `Post #${req.params.id} was successfuly deleted` });
        }


    } catch (err) {
        res.json({ error: err })
    }

})



module.exports = router;