//----->>> Libraries & Tools <<<------
const express = require('express');
const router = express.Router();


//Routes
const posts = require('./posts');


//Mounting routes.
router.use('/posts', posts);





module.exports = router;