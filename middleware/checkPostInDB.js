const Post = require('../db/models/Post');
const Category = require('../db/models/Category');

const checkPostInDB = async(req, res, next) => {

    try {
        //Search for record in db
        const searchDB = await Post.findAll({
                include: {
                    model: Category,
                    attributes: ['id', 'descripcion']
                },
                attributes: ['id', 'titulo', 'imagen', 'fecha'],
                where: {
                    id: req.params.id
                }
            })
            //Handle 404 status Response
        if (searchDB.length === 0) {
            return res.status(404).json({ error: `Post #${req.params.id} not found` });

        } else {
            res.searchDB = searchDB;
            next()
        }

    } catch (err) {
        return res.json(err)
    }

}

module.exports = checkPostInDB;