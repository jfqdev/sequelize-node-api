const axios = require('axios');
const isValidImage = async(req, res, next) => {

    try {
        const resp = await axios.get(req.body.imagen)
        let regex = /image/;
        const isImage = regex.test(resp.headers['content-type']) //true if  is an image.

        if (isImage && resp.status === 200) {
            next()
        } else if (!isImage) {
            return res.status(404).json({ error: `Not an image` });
        } else {
            return res.status(404).json({ error: `Image not available` });
        }

    } catch (err) {
        res.json(err)
    }


}


module.exports = isValidImage;